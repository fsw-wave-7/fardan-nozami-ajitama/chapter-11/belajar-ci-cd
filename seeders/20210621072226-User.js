module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Users',
      [
        {
          username: 'Robby',
          email: 'robby@gmail.com',
          password:
            '$2b$10$80AUG8GPreWX0bTtHwFmveq3Yv9pjUYASWwyuYoODoUam6tlffsOW',
          biodata: 'I am a web developer expert',
          social_media_url: 'instagram.com/robby',
          city_id: 05,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: 'Niko',
          email: 'niko@gmail.com',
          password:
            '$2b$10$80AUG8GPreWX0bTtHwFmveq3Yv9pjUYASWwyuYoODoUam6tlffsOW',
          biodata: 'I am a web developer expert',
          social_media_url: 'instagram.com/niko',
          city_id: 07,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: 'Fardan',
          email: 'fardan@gmail.com',
          password:
            '$2b$10$80AUG8GPreWX0bTtHwFmveq3Yv9pjUYASWwyuYoODoUam6tlffsOW',
          biodata: 'I am a web developer expert',
          social_media_url: 'instagram.com/fardan',
          city_id: 03,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: 'Luthfi',
          email: 'luthfi@gmail.com',
          password:
            '$2b$10$80AUG8GPreWX0bTtHwFmveq3Yv9pjUYASWwyuYoODoUam6tlffsOW',
          biodata: 'I am a web developer expert',
          social_media_url: 'instagram.com/luthfi',
          city_id: 04,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: 'Tito',
          email: 'tito@gmail.com',
          password:
            '$2b$10$80AUG8GPreWX0bTtHwFmveq3Yv9pjUYASWwyuYoODoUam6tlffsOW',
          biodata: 'I am a web developer expert',
          social_media_url: 'instagram.com/tito',
          city_id: 02,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: 'Angie',
          email: 'angie@gmail.com',
          password:
            '$2b$10$80AUG8GPreWX0bTtHwFmveq3Yv9pjUYASWwyuYoODoUam6tlffsOW',
          biodata: 'I am a web developer expert',
          social_media_url: 'instagram.com/angie',
          city_id: 01,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: 'User7',
          email: 'user7@gmail.com',
          password:
            '$2b$10$80AUG8GPreWX0bTtHwFmveq3Yv9pjUYASWwyuYoODoUam6tlffsOW',
          biodata: 'I am a web developer expert',
          social_media_url: 'instagram.com/user7',
          city_id: 09,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: 'User8',
          email: 'user8@gmail.com',
          password:
            '$2b$10$80AUG8GPreWX0bTtHwFmveq3Yv9pjUYASWwyuYoODoUam6tlffsOW',
          biodata: 'I am a web developer expert',
          social_media_url: 'instagram.com/user8',
          city_id: 06,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: 'User9',
          email: 'user9@gmail.com',
          password:
            '$2b$10$80AUG8GPreWX0bTtHwFmveq3Yv9pjUYASWwyuYoODoUam6tlffsOW',
          biodata: 'I am a web developer expert',
          social_media_url: 'instagram.com/user9',
          city_id: 06,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: 'User10',
          email: 'user10@gmail.com',
          password:
            '$2b$10$80AUG8GPreWX0bTtHwFmveq3Yv9pjUYASWwyuYoODoUam6tlffsOW',
          biodata: 'I am a web developer expert',
          social_media_url: 'instagram.com/user10',
          city_id: 05,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: 'User11',
          email: 'user11@gmail.com',
          password:
            '$2b$10$80AUG8GPreWX0bTtHwFmveq3Yv9pjUYASWwyuYoODoUam6tlffsOW',
          biodata: 'I am a web developer expert',
          social_media_url: 'instagram.com/user11',
          city_id: 04,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
